#!/usr/bin/python
##################################################################################################
#   Author:         Kevin "syscall" Agosto 
#   Contact:        kevagost@uat.edu
#   Description:    The purpose of this script is to automate most of the information gathering
#                   process.
#   Version:        1.1
##################################################################################################
#                                       Libraries
import sys
import urllib2
import cookielib
import re
import subprocess
from HTMLParser import HTMLParser

###################################################################################################
#                                       Global Variables

outputFile = ""             # The name for our output file
hostBin = "/usr/bin/host"   # Path to host binary
whoisBin = "/usr/bin/whois" # Path to whois binary
hostsdict = "dns.txt"       # Dictionary of common hosts

###################################################################################################
#                                       Program Functions

# A custom class to fetch all input data
class tagParser(HTMLParser):

    # Initialize object
    def __init__(self):
        HTMLParser.__init__(self)
        self.tag_results = {}

    # Handle the tags
    def handle_starttag(self, tag, attrs):

        # Fetch all input tags
        if tag == "input":
            tag_name = None
            tag_value = None

            # Get its attributes
            for name,value in attrs:
                if name == "name":
                    tag_name = value
                if name == "value":
                    tag_value = value
            
            # Add tag to tag_results if there is one
            if tag_name is not None:
                self.tag_results[tag_name] = tag_value


# Fatal Error function, returns -1
def fatal_error(errmsg):
    print(errmsg)
    exit(-1)

# Displays the banner
def displayBanner():
    print """
______                      ______                
| ___ \                     | ___ \               
| |_/ /___  ___ ___  _ __   | |_/ /_      ___ __  
|    // _ \/ __/ _ \| '_ \  |  __/\ \ /\ / / '_ \ 
| |\ \  __/ (_| (_) | | | | | |    \ V  V /| | | |
\_| \_\___|\___\___/|_| |_| \_|     \_/\_/ |_| |_|

            Version 1.1
     By: Kevin "syscall" Agosto
          kevagost@uat.edu
"""



# Write information to file
def save2file(msg):

    # Attempt to write string to file
    try:
        output = open(outputFile, "a")
        output.write(msg)
    except:
        fatal_error("[!!] Could not write to file!\n")

    output.close()

# Obtain Network information 
def gatherNetworkInfo(target):
    
    # Use the linux host and whois tools via subprocess
    host_process = subprocess.Popen([hostBin, target], stdout=subprocess.PIPE, shell=False)
    host_output = host_process.stdout.read()
    
    # Split to obtain IP only, delimiter of space field 3
    host_ip = host_output.split(' ')[3]
    host_ip = host_ip.rstrip()

    print("\n\n\n[*] Target: %s has IP address of %s\n") % (target, host_ip)

    # Save IP to file
    save2file("<center><h1>"+target+" Recon Results</h1></center>\n<p>Has IPv4 Address: <b>"+host_ip+"</b></p><br>\n")
    
    # Perform whois lookup. 
    print("[*] Fetching WHOIS Information and saving to output file\n") 
    save2file("<center><h2> Whois Information </h2></center>\n<p>")
    whois_proc = subprocess.Popen([whoisBin, host_ip], stdout=subprocess.PIPE, shell=False)
    whois_output = whois_proc.stdout.readlines() 

    # Iterate through each output line and save to file
    for line in whois_output:
        save2file(line+"<br>")
   
    # Close tag
    save2file("</p>\n")


    # Perform reverse lookups on class C network
    print("[*] Performing Reverse Lookup on target %s/24 ***EXPERIMENTAL***\n")%(host_ip)
    save2file("<center><h2> Reverse Lookup </h2></center>\n") 
    target_class = re.findall(r"(\d*\.\d*\.\d*\.)", host_ip)
    counter = 1
    while(counter != 254):
        host_proc = subprocess.Popen([hostBin, target_class[0].rstrip()+str(counter)], stdout=subprocess.PIPE, shell=False)
        
        # Verify if we have a hit
        host_output = host_proc.stdout.read()
        if "domain name pointer" in host_output:
            save2file("\n<br><p>"+host_output+"</p><br>\n")
        counter += 1


    # Perform BRUTE host lookup
    print("[*] Performing Brute host lookup\n")
    save2file("<center><h2> Bruteforce Host Lookup </h2></center>\n")

    # remove www if supplied
    if "www" in target:
        domain = re.sub(r"www.", "", target)
    else:
        domain = target

    # Open host dictionary and commence bruteforce
    try:
        brute_hosts = open(hostsdict, "rb")
        for brute_host in brute_hosts:
            brute_host = brute_host.rstrip()+"."+domain
            hosts_proc = subprocess.Popen([hostBin, brute_host], stdout=subprocess.PIPE, shell=False)
            output = hosts_proc.stdout.read()

            # Save to file if host exists
            if "has address" in output:
                save2file("<p>"+output+"</p><br>\n")

    except:
        fatal_error("[!!] Error while performing brute host lookup, check if dictionary file exists!\n")


# Gather website information
def gatherSiteInfo(target):
 
    # Declare variables
    cookiejar = cookielib.FileCookieJar("pentestcookie")                        # Cookie jar object
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookiejar))       # Create an opener
    opener.addheaders = [("User-Agent", "Mozilla/5.0"), ("Referrer", target)]   # Add relevant headers
    results = []                                                                # List to store results

    print("[*] Gathering website information\n")

    # Verify if user supplied the protocol
    if "http://" in target or "https://" in target:
        try:
            req = opener.open(target)
            contents = req.read().decode('utf-8')
            headers = req.info()
        except:
            fatal_error("[!!] Cannot connect to target\n")
    
    else:
        try:
            req = opener.open("http://"+target+"/")
            contents = req.read().decode('utf-8')
            headers = req.info()
        except:
            fatal_error("[!!] Cannot connect to target\n")

    # Start storing the information found
    if "script" in contents:
        results.append("[*] Found language: JavaScript\n")
    if "css" in contents:
        results.append("[*] Found language: CSS\n")
    if "ajax" in contents:
        results.append("[*] Found language: AJAX\n")
    if "xml" in contents:
        results.append("[*] Found language: XML\n")
    if "xmlrpc" in contents:
        results.append("[*] Found xmlrpc\n")
    if "wp-content" in contents:
        results.append("[*] Found wp-content (WordPress!)\n")
    if "wp-admin" in contents:
        results.append("[*] FOUND wp-admin directory!\n")

    # Save found languages to file
    save2file("\n<center><h2> Languages Found </h2></center>\n<p>\n")
    for item in results:
        save2file(item+"<br>")
    save2file("</p><br>\n")

    # Find all form input names and values
    parser = tagParser()
    parser.feed(contents)
    post_data= parser.tag_results

    # Iterate through form data
    save2file("<center><h2> Form Input </h2></center><p>\n")
    for key,value in post_data.iteritems():
        if key is None:
            key = "None"
        if value is None:
            value = "None"

        # Save findings
        save2file("[*] Form input name '"+key+"' value='"+value+"'<br>\n")
       
    # Save Header information
    print("[*] Fetching header information\n")
    results = []       # Store header findings

    save2file("<center><h2> Webserver Headers </h2></center><br>\n")
    results.append("[*] Server: "+headers['server']+"\n")
 
    if headers['x-powered-by']:
        results.append("[*] X-Powered-By: "+headers['x-powered-by']+"\n")

    # save to file
    for item in results:
        save2file("<p>"+item+"</p><br>\n")


#####################################################################################################
#                                   MAIN PROGRAM

if __name__ == "__main__":

    # Display our banner
    displayBanner()

    # Prompt for target information
    target = raw_input("[+] Enter the website you wish to perform recon: ")
    target = target.rstrip()

    # Prompt user for output filename
    outputFile = raw_input("[+] Enter the output filename (please include the .html extension): ")

    # Add html tag
    save2file("<html>\n<body>\n<title>Scan Results</title>\n")

    # Perform recon functions
    gatherNetworkInfo(target)
    gatherSiteInfo(target)

    # Add end tags
    save2file("</body>\n</html>")

    # Print Final Message
    print("[-] Done!!!! Please open %s with any modern browser ;)\n") % (outputFile)

# EOF
